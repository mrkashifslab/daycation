import nconf from 'nconf'
import { expect } from 'chai'
import { callQuery } from './../global'

nconf.file({ file: './tests/dump/global.json' })
const userToken = nconf.get('userToken')

describe('Places Tests', () => {

    it('Getting all places before user adding a place | Checking for - (Null)', async () => {
      const res = await callQuery(`
      query {
        getAllPlaces {
          _id
        }
      }
      `, userToken)
      expect(res.data.getAllPlaces).to.be.null
    })
})
