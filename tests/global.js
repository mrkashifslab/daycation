require('dotenv').config({ path: '.env' })
import axios from 'axios'

export const endpoint = `http://localhost:${process.env.TEST_PORT}/graphql`

export const callQuery = async (passedQuery, token) => {
  try {
    const res = await axios.post(endpoint, {
      query: passedQuery
    }, {
      headers: {'authorization': !token ? '' : token}
    })
    return res.data
  } catch (error) {
    return error.response.data
  }
}

export default {
  endpoint,
  callQuery
}
