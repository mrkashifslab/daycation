import nconf from 'nconf'
import { expect } from 'chai'
import { callQuery } from './../global'

describe('Authentication Tests', () => {

  describe('Signup Tests', () => {

    it('Signing up a user with missing name | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        signup(data: {name: "", email: "a@a.com", username: "a", password: "123456"})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),
  
    it('Signing up a user with missing email | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        signup(data: {name: "Ssething", email: "", username: "a", password: "123456"})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),

    it('Signing up a user with missing username | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        signup(data: {name: "Ssething", email: "a@a.com", username: "", password: "123456"})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),

    it('Signing up a user with missing password | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        signup(data: {name: "Ssething", email: "a@a.com", username: "aaa", password: ""})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),

    it('Signing up a user with all required feilds | Checking for -  (Success)', async () => {
      const res = await callQuery(`
      mutation {
        signup(data: {name: "John Doe", email: "john@john.com", username: "johndoe", password: "123456"})
      }
      `)
      const data = {name: "John Doe", email: "john@john.com", username: "johndoe", password: "123456"}

      nconf.file({ file: './tests/dump/global.json' })
      nconf.set('createdUser', data);
      nconf.save();
      expect(res.data.signup).to.be.true
    })
  }),

  describe('Login Tests', () => {

    it('Logging in without username | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        login(data: {username: "", password: "123456"})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),

    it('Logging in without password | Checking for -  (Error)', async () => {
      const res = await callQuery(`
      mutation {
        login(data: {username: "meh", password: ""})
      }
      `)
      expect(res.errors[0].message).to.match(/Feilds cannot be empty/)
    }),

    it('Logging in with incorrect username | Checking for -  (Error)', async () => {
      nconf.file({ file: './tests/dump/global.json' })
      const username = nconf.get('createdUser:username') + '1'
      const password = nconf.get('createdUser:password')
      const res = await callQuery(`
      mutation {
        login(data: {username: "${username}", password: "${password}"})
      }
      `)
      expect(res.errors[0].message).to.have.string('Incorrect username/password')
    }),

    it('Logging in with incorrect password | Checking for -  (Error)', async () => {
      nconf.file({ file: './tests/dump/global.json' })
      const username = nconf.get('createdUser:username')
      const password = nconf.get('createdUser:password') + '1'
      const res = await callQuery(`
      mutation {
        login(data: {username: "${username}", password: "${password}"})
      }
      `)
      expect(res.errors[0].message).to.have.string('Incorrect username/password')
    }),

    it('Logging in Signed up user to get token | Checking for -  (Success)', async () => {
      nconf.file({ file: './tests/dump/global.json' })
      const username = nconf.get('createdUser:username')
      const password = nconf.get('createdUser:password')
      const res = await callQuery(`
      mutation {
        login(data: {username: "${username}", password: "${password}"})
      }
      `)
      nconf.file({ file: './tests/dump/global.json' })
      nconf.set('userToken', res.data.login)
      nconf.save()
      expect(res.data.login).to.have.string
    })
  })
})
