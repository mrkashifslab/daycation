import { Root } from './../root'
import { Place } from './../place'
import serviceModel from './../../models/service'

export const Service = `
  directive @isUserAuth on QUERY | FIELD
  directive @isAdminWithRole(role: [String]) on QUERY | FIELD

  # A service object type
  type Service {
    # ID of the service in DB
    _id: ID!
    # Name of the service
    name: String!
    # Description of service
    description: String!
    # Pricing of the service /hr
    price: Float!
    # Place where the service belongs
    place: ID!
    # Time at which service document was created
    createdAt: String!
    # Time at which service document was updated
    updatedAt: String!
  }

  extend type Place {
    # Services offered by a place
    services: [Service]
  }

  input newServiceInput {
    # Enter name of the service
    name: String!
    # Enter description of service
    description: String!
    # Enter ID of the place it belongs
    place: ID!
    # Pricing of the service /hr
    price: Float!
  }

  extend type Mutation {
    # Add a service
    newService(data: newServiceInput!): Service! @isUserAuth
  }
`

const resolvers = {
  Place: {
    async services(place) {
      return serviceModel.find({ place: place._id }).lean()
    }
  },
  Mutation: {
    newService: async (src, { data }, { user }) => {
      return await (new serviceModel(data)).save()
    }
  }
}

export default {
  types: [Service, Place, Root],
  resolvers
}
