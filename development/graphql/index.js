import { makeExecutableSchema } from 'graphql-tools';
import _ from 'lodash';
import { directiveResolvers } from './directives/auth'
import Root from './root'
import Auth from './auth'
import User from './user'
import Place from './place'
import Service from './service'

export default makeExecutableSchema({
  typeDefs: [...Root.types, ...Auth.types, ...User.types, ...Place.types, ...Service.types],
  resolvers: _.merge(Root.resolvers, Auth.resolvers, User.resolvers, Place.resolvers, Service.resolvers),
  directiveResolvers
});
