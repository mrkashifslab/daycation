export const Root = `
  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
  # The main query type
  type Query {
    # Returns a string containing pong for testing
    ping: String!
  }
  # The main mutation interface
  type Mutation {
    # Returns a string containing pong for testing
    ping: String!
  }
  # The main subscription interface
  type Subscription {
    # Return number of seconds the websocket is active
    ping: Int!
  }
`;

const resolvers = {
  Query: {
    ping: () => 'pong'
  },
  Mutation: {
    ping: () => 'pong'
  },
  Subscription: {
    ping: () => {
      var n = 0
      setInterval(function () {
        n++
      }, 1000)
      return n
    }
  }
};

export default {
  types: [Root],
  resolvers
}
