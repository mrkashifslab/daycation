import jwt from 'jsonwebtoken'
import { AuthorizationError } from './../../handlers/errors'
import adminUserModel from './../../../models/adminUser'

export const directiveResolvers = {
  isUserAuth: (next, source, args, context) => {
    const token = context.headers.authorization;
    if (!token) {
      throw new AuthorizationError({
        message: "You must supply a JWT for authorization!"
      });
    }
    try {
      const decoded = jwt.verify(
        token,
        process.env.JWT_SECRET
      );
      context.user = decoded;
      return next();
    } catch (err) {
      throw new AuthorizationError({
        message: "You are not authorized."
      });
    }
  },
  // isAdminAuth: (next, source, args, context) => {
  //   const token = context.headers.authorization
  //   if (!token) {
  //     throw new AuthorizationError({
  //       message: "You must supply a JWT for authorization!"
  //     });
  //   }
  //   try {
  //     const decoded = jwt.verify(
  //       token,
  //       process.env.JWT_SECRET
  //     )
  //     if (decoded.admin === true) {
  //       context.user = decoded;
  //       return next();
  //     }
  //     throw new AuthorizationError({
  //       message: "You're not an authorized admin"
  //     })
  //   } catch (err) {
  //     throw new AuthorizationError({
  //       message: "You are not an authorized admin."
  //     });
  //   }
  // },
  isAdminWithRole: async (next, source, args, context) => {
    const token = context.headers.authorization;
    const expectedRoles = args.role;
    if (!token) {
      throw new AuthorizationError({
        message: 'You must supply a JWT for authorization!'
      });
    }
    try {
      const decoded = jwt.verify(
        token,
        process.env.JWT_SECRET
      );
      const { roles } = await adminUserModel.findOne( { 'user': decoded._id }, { roles: 1 } ).lean()
      if (expectedRoles.includes(...roles) || (expectedRoles.includes('All') && roles.length !== 0)) {
        return next();
      }
      return Promise.reject(
        new AuthorizationError({
          message: `You are not authorized. Your roles: ${roles.join(', ')}, Expected either of these roles: ${expectedRoles.join(', ')}`
        })
      );
    } catch (err) {
      return Promise.reject(
        new AuthorizationError({
          message: `You are not authorized. Expected roles: ${expectedRoles.join(', ')}`
        })
      );
    }
  }
};
