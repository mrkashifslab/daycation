import { Root } from './../root'
import userModel from './../../models/user'

export const User = `
  directive @isUserAuth on QUERY | FIELD
  directive @isAdminAuth on QUERY | FIELD

  # A user data object type
  type User {
    # ID in db of the user
    _id: ID!
    # Fullname of the user
    name: String!
    # Email of the user
    email: String!
    # Username of the user
    username: String!
    # Places pinned by the user
    pins: [ID!]
    # Time at which user document was created
    createdAt: String!
    # Time at which user document was updated
    updatedAt: String!
  }

  extend type Query {
    # View my profile
    myProfile: User! @isUserAuth
  }
`

const resolvers = {
  Query: {
    myProfile: async (_req, args, { user }) => {
      return await userModel.findById(user._id)
    }
  }
}

export default {
  types: [User, Root],
  resolvers
}
