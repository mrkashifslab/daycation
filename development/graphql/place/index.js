import { Root } from './../root'
import { User } from './../user'
import placeModel from './../../models/place'
import { omit } from 'lodash'

export const Place = `
  directive @isUserAuth on QUERY | FIELD
  directive @isAdminWithRole(role: [String]) on QUERY | FIELD

  # A location object type
  type Location {
    # Co-ordinates of a place
    coordinates: [Float!],
    # Address of a place
    address: String!
  }

  # A place data object type
  type Place {
    # ID of the place in DB
    _id: ID!
    # Name of the place
    name: String!
    # Description of place
    description: String!
    # Owner of place
    owner: ID!
    location: Location!
    # Time at which place document was created
    createdAt: String!
    # Time at which place document was updated
    updatedAt: String!
  }

  extend type User {
    # Places owned by the user
    myPlaces: [Place]
  }

  input newPlaceInput {
    # Enter name of the place
    name: String!
    # Enter description of place
    description: String!
    # Enter address of the place
    address: String!
    # Enter longitude of the place
    longitude: Float!
    # Enter latitude of the place
    latitude: Float!
  }

  input radiusInput {
    # Longitude of your position
    longitude: Float!
    # Latitude of your position
    latitude: Float!
    # Radius in Km
    radius: Float!
  }

  extend type Query {
    # View all places I own
    getAllPlacesIOwn: [Place!] @isUserAuth
    # View all places within radius
    getAllPlacesWithinRadius(data: radiusInput!): [Place!]
  }

  extend type Mutation {
    # Add a place
    newPlace(data: newPlaceInput!): Place! @isUserAuth
  }
`

const resolvers = {
  User: {
    async myPlaces(user) {
      return await placeModel.find({ owner: user._id }).lean()
    }
  },
  Mutation: {
    newPlace: async (src, { data }, { user }) => {
      data.owner = user._id
      data.location = {
        address: data.address,
        coordinates: [data.longitude, data.latitude]
      }
      const place = omit(data, ["longitude", "latitude", "address"])
      const writePlace = await (new placeModel(place)).save()
      return writePlace
    }
  },
  Query: {
    // getAllPlacesIOwn: async (src, args, { user }) => {
    //   // return await placeModel.find({ owner: user._id }).lean()
    //   return await myPlaces(user)
    // },
    getAllPlacesWithinRadius: async (src, { data }, { user }) => {
      return await placeModel.find({location: { $geoWithin: {$centerSphere: [ [data.longitude, data.latitude], data.radius / 6378.1  ] } }}).lean()
    }
  }
}

export default {
  types: [Place, User, Root],
  resolvers
}
