import { Root } from './../root'
import { User } from './../user'
import userModel from './../../models/user'
import adminUserModel from './../../models/adminUser'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import _ from 'lodash'

export const Authorization = `
  input createUserInput {
    # Fullname of the user
    name: String!
    # Email of the user
    email: String!
    # Username of the user
    username: String!
    # Password for the account
    password: String!
  }

  input loginUserInput {
    # Username of the user
    username: String!
    # Password for the account
    password: String!
  }

  extend type Mutation {
    # New user signup
    signup(data: createUserInput!): Boolean!
    # User Login
    login(data: loginUserInput!): String!
  }
`

const notEmpty = (data) => {
  const check = Object.values(data).every(x => x !== '')
  if (!check) {
    throw new Error('Feilds cannot be empty')
  }
}

const checkUserExists = async (username, email) => {
  const result =  await userModel.findOne({ $or: [ { 'username': username }, { 'email': email } ] }, { _id: 1, password: 1 }).lean()
  return result
}

const resolvers = {
  Mutation: {
    signup: async (_req, {data}, context) => {
      notEmpty(data)
      let check = await checkUserExists(data.username, data.email)
      if (!check) {
        const newUser = await (new userModel(data)).save()
        if (!newUser) {
          throw new Error('Error creating a user')
        }
        return true
      }
      throw new Error('Username or Email is already being used')
    },
    login: async (_req, {data}, context) => {
      notEmpty(data)
      let check = await checkUserExists(data.username)
      if (!check) {
        throw new Error('Incorrect username/password')
      }
      const valid = await bcrypt.compare(data.password, check.password)
      if (!valid) {
        throw new Error('Incorrect username/password')
      }
      return jwt.sign( _.pick(check, "_id") , process.env.JWT_SECRET, { expiresIn: '1y' })
    }
  }
}

export default {
  types: [Authorization, User, Root],
  resolvers
}
