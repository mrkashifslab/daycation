import mongoose from 'mongoose'
import 'babel-polyfill'
import app from './app'
import userModel from './models/user'
import adminUserModel from './models/adminUser'

const [major, minor] = process.versions.node.split('.').map(parseFloat)
if (major < 8 || (major === 8 && minor <= 5)) {
  console.log('You\'re running an older version of nodeJS...\nUpdate your nodeJS version to the latest to run this app by going to nodeJS.org');
  process.exit()
}

require('dotenv').config({ path: '.env' })

const dbCheck = (!process.env.TEST_DATABASE) ? process.env.DATABASE : process.env.TEST_DATABASE
mongoose.connect(dbCheck)
mongoose.Promise = global.Promise
mongoose.connection.on('error', (err) => {
  console.error(`${err.message}`);
})
mongoose.connection.once('open', () => {
  console.log(`Connected to mongoDB at ${dbCheck}`);
})

async function createOneUser() {
  const condi = await userModel.findOne({'email': 'kashif_9849@hotmail.com'}).lean()
  if (!condi) {
    const user = {
      name: "Kashif",
      email: "kashif_9849@hotmail.com",
      username: "mrkashif",
      password: "123456"
    }
    console.log(`\nInitializing user insertion`)
    const write = await (new userModel(user)).save()
    console.log(`\nCreated user with _id: ${write._id} in database`)
    const setAdmin = {user: write._id, roles: ['master']}
    const writeAdmin = await (new adminUserModel(setAdmin)).save()
    console.log(`\nCreated Master Admin with _id: ${writeAdmin._id} in database`);
  } else {
    console.log(`\nA User & Master Admin Exists in database and has the following credentials -\n\nusername: mrkashif\npassword: 123456`)
  }
}

createOneUser()

const portCheck = (!process.env.TEST_PORT) ? process.env.PORT : process.env.TEST_PORT
app.set('port', portCheck || 5555)
const server = app.listen(app.get('port'), () => {
  console.log(`Daycation running on port ${server.address().port}`);
})
