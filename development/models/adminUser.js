import mongoose from 'mongoose'
const Schema = mongoose.Schema

const adminUserSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'user',
    required: 'User id to give admin access required'
  },
  roles: {
    type: [String],
    required: 'At least one role is required'
  }
})

adminUserSchema.index({user: 1})

export default mongoose.model('admin-user', adminUserSchema)
