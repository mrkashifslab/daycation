import mongoose from 'mongoose'
const Schema = mongoose.Schema

const serviceSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: `Please provice a name for this service`
  },
  description: {
    type: String,
    trim: true,
    required: `Describe what this service is about`
  },
  place: {
    type: Schema.ObjectId,
    ref: 'place'
  },
  price: {
    type: Number,
    trim: true,
    required: `Please provide a /hr pricing for this service`
  }
}, {timestamps: true})

serviceSchema.index({
  name: 'text',
  description: 'text'
});

export default mongoose.model('service', serviceSchema)
