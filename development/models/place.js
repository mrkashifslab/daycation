import mongoose from 'mongoose'
const Schema = mongoose.Schema

const placeSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: `Please provice a name for this place`
  },
  description: {
    type: String,
    trim: true,
    required: `Describe about the place`
  },
  owner: {
    type: Schema.ObjectId,
    ref: 'user'
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'You must supply coordinates!'
    }],
    address: {
      type: String,
      required: 'You must supply an address!'
    }
  },
}, {timestamps: true}, {toJSON: { virtuals: true }, toObject: { virtuals: true }})

placeSchema.index({
  name: 'text',
  description: 'text'
});

placeSchema.index({ location: '2dsphere' });

export default mongoose.model('place', placeSchema)
