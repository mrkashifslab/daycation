import mongoose from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcrypt'
const Schema = mongoose.Schema
const salt_factor = 12

const userSchema = new Schema({
  name: {
    type: String,
    required: 'Please supply a name',
    trim: true
  },
  username: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: 'Please supply a username'
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: [validator.isEmail, 'Invalid Email Address'],
    required: 'Please supply an email address'
  },
  password: {
    type: String,
    required: 'Please supply a password'
  },
  pins: [{
    type: Schema.ObjectId,
    ref: 'service'
  }]
}, {
  timestamps: true
});

userSchema.pre('save', async function (next) {
  var user = this;
  if (!user.isModified('password')) return next();
  user.password = await bcrypt.hash(user.password, salt_factor)
});

export default mongoose.model('user', userSchema)
