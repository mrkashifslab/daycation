import express from 'express'
import mongoose from 'mongoose'
import graphqlHTTP from 'express-graphql'
import cors from 'cors'
import bodyParser from 'body-parser'
import schema from './graphql'

const api = express()

api.use(cors())

api.use(express.static(__dirname))

api.use('/graphql', bodyParser.json(), graphqlHTTP (() => ({
  schema,
  graphiql: true
})));

api.get('/', (req, res) => {
  res.send('Daycation is on!')
})

module.exports = api;
